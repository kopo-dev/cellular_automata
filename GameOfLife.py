import random
import time
from Grid import Grid


class GameOfLife():

    def __init__(self, width, height, cell_size):
        self.grid = Grid(width, height, cell_size)
        self.generate_cells()
        self.grid.draw()

    def generate_cells(self):
        for row in range(0, self.grid.grid_height):
            self.grid.cells.append([])
            for col in range(0, self.grid.grid_width):
                if random.random() < 0.2:
                    self.grid.cells[row].append(1)
                else:
                    self.grid.cells[row].append(0)

    def run_rules(self):
        new_grid = []
        for row in range(0, self.grid.grid_height):
            new_grid.append([])
            for col in range(0, self.grid.grid_width):
                new_grid[row].append(0)
                neighbours = self.grid.getNeightbours(col, row)
                lives = neighbours.count(1)
                if (self.grid.cells[row][col] == 1 and (lives < 2 or lives > 3)):
                    new_grid[row][col] = 0
                elif(self.grid.cells[row][col] == 0 and lives == 3):
                    new_grid[row][col] = 1
                else:
                    new_grid[row][col] = self.grid.cells[row][col]
        self.grid.cells = new_grid
        
