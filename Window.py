import pyglet
import time
from Grid import Grid
from GameOfLife import GameOfLife
from ForestFire import ForestFire
from SandPile import SandPile

class Window(pyglet.window.Window):
    def __init__(self, width = 800, height = 600, cell_size = 5, simu = "GameOfLife"):
        super().__init__(width, height)
        if (simu == "GameOfLife"):
            self.simu = GameOfLife(width, height, cell_size)
        elif (simu == "ForestFire"):
            self.simu = ForestFire(width, height, cell_size)
        elif (simu == "SandPile"):
            self.simu = SandPile(width, height, cell_size)
        pyglet.clock.schedule_interval(self.update, 0.05)
        
    def on_draw(self):
        self.clear()
        self.simu.grid.draw()

    def update(self, dt):
        self.simu.run_rules()