import random
from Grid import Grid

class SandPile():
    def __init__(self, width, height, cell_size):
        self.grid = Grid(width, height, cell_size)
        self.init_grid()
        self.grid.draw()

    def init_grid(self):
        for row in range(0, self.grid.grid_height):
            self.grid.cells.append([])
            for col in range(0, self.grid.grid_width):
                self.grid.cells[row].append(0)
        y = int(self.grid.grid_height / 2)
        x = int(self.grid.grid_width / 2)
        self.grid.cells[y][x] = 100000000000

    def run_rules(self):
        new_grid = []
        for row in range(0, self.grid.grid_height):
            new_grid.append([])
            for col in range(0, self.grid.grid_width):
                new_grid[row].append(0)
                if (self.grid.cells[row][col] < 4):
                    new_grid[row][col] = self.grid.cells[row][col]
        for row in range(0, self.grid.grid_height):
            for col in range(0, self.grid.grid_width):
                num = self.grid.cells[row][col]
                if (num >= 4):
                    new_grid[row][col] += (num - 4)
                    if (col - 1 < 0):
                        minus_col = self.grid.grid_width - 1
                    else:
                        minus_col = col - 1
                    if (col + 1 >= self.grid.grid_width):
                        plus_col  = 0
                    else:
                        plus_col = col + 1
                    if (row - 1 < 0):
                        minus_row = self.grid.grid_height - 1
                    else:
                        minus_row = row - 1
                    if (row + 1 >= self.grid.grid_height):
                        plus_row  = 0
                    else:
                        plus_row = row + 1
                    new_grid[minus_row][col] += 1
                    new_grid[plus_row][col] += 1
                    new_grid[row][minus_col] += 1
                    new_grid[row][plus_col] += 1
        self.grid.cells = new_grid
        
