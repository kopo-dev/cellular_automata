class Cell():
    
    green = (0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255) 
    red = (255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255) 
    blue = (0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255)

    def __init__(self, status, color):
        self.status = status
        if color == "red":
            self.color = self.red
        elif color == "green":
            self.color = self.green
        elif color == "blue":
            self.color = self.blue



    