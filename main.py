import pyglet

from Window import Window

def menu():
    simu = ""
    width = 900
    height = 600
    cell_size = 10
    print("Welcome !")
    while (True):
        print("Please choose an automaton:\n1) Conway's Game of Life\n2) Forest Fire\n3) Sand Pile")
        try:
            choice = int(input("--> "))
        except:
          print("Your choice must be a number")
          continue
        if (choice > 3):
            print("This choice doesn't exist yet")
            continue
        break
    if (choice == 1):
        simu = "GameOfLife"
    elif(choice == 2):
        simu = "ForestFire"
    elif(choice == 3):
        simu = "SandPile"
    while(True):
        print("Please choose the width of the window\nPress Enter for default settings")
        choice = input("--> ")
        if(choice == ''):
            break
        else:
            try:
                choice = int(choice)
            except:
              print("Your choice must be a number")
              continue
        width = choice
        break
    print("%s w: %d" % (simu, width))
    while(True):
        print("Please choose the height of the window\nPress Enter for default settings")
        choice = input("--> ")
        if(choice == ''):
            break
        else:
            try:
                choice = int(choice)
            except:
              print("Your choice must be a number")
              continue
        height = choice
        break
    print("%s w: %d h: %s" % (simu, width, height))
    while(True):
        print("Please choose the size of the cells\nPress Enter for default settings")
        choice = input("--> ")
        if(choice == ''):
            break
        else:
            try:
                choice = int(choice)
            except:
              print("Your choice must be a number")
              continue
        cell_size = choice
        break
    print("%s w: %d h: %d cell_size: %d" % (simu, width, height, cell_size))
    window = Window(width, height, cell_size, simu)


if __name__ == '__main__':
    menu()
    pyglet.app.run()
 