import pyglet

class Grid():
    def __init__(self, width, height, cell_size):
        self.width = width
        self.height = height
        self.cell_size = cell_size
        self.grid_width = int(width / cell_size)
        self.grid_height = int(height / cell_size)
        self.cells = []
    
    def getNeightbours(self, x, y):
        neightbours = []
        if (x - 1 < 0):
            minus_x = self.grid_width - 1
        else:
            minus_x = x - 1
        if (x + 1 >= self.grid_width):
            plus_x  = 0
        else:
            plus_x = x + 1
        if (y - 1 < 0):
            minus_y = self.grid_height - 1
        else:
            minus_y = y - 1
        if (y + 1 >= self.grid_height):
            plus_y  = 0
        else:
            plus_y = y + 1
        neightbours.append(self.cells[minus_y][minus_x])
        neightbours.append(self.cells[minus_y][x])
        neightbours.append(self.cells[minus_y][plus_x])
        neightbours.append(self.cells[y][minus_x])
        neightbours.append(self.cells[y][plus_x])
        neightbours.append(self.cells[plus_y][minus_x])
        neightbours.append(self.cells[plus_y][x])
        neightbours.append(self.cells[plus_y][plus_x])
        return neightbours
        
    def drawGrid(self):
        batch = pyglet.graphics.Batch()
        pyglet.gl.glLineWidth(1)
        for row in range(0, self.grid_height):
            batch.add(2, pyglet.gl.GL_LINES, None,('v2i', (0, row * self.cell_size, self.width, row * self.cell_size)))
        for col in range(0, self.grid_width):
            batch.add(2, pyglet.gl.GL_LINES, None,('v2i', (col * self.cell_size, 0, col * self.cell_size, self.height)))
        batch.draw()

    def draw(self):
        green = (0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255, 0, 255)
        red = (255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255) 
        un = (0, 185, 63, 255, 0, 185, 63, 255, 0, 185, 63, 255, 0, 185, 63, 255)
        deux = (0, 104, 255, 255,0, 104, 255, 255,0, 104, 255, 255, 0, 104, 255, 255)
        trois = (122, 0, 229, 255, 122, 0, 229, 255, 122, 0, 229, 255, 122, 0, 229, 255)
              
        botch = pyglet.graphics.Batch()
        for row in range(0, self.grid_height):
            for col in range(0, self.grid_width):
                if self.cells[row][col] != 0:
                    cell_coords = (col * self.cell_size, row * self.cell_size,
                                   col * self.cell_size, row * self.cell_size + self.cell_size,
                                   col * self.cell_size + self.cell_size, row * self.cell_size + self.cell_size,
                                   col * self.cell_size + self.cell_size, row * self.cell_size
                                   )
    
                    if (self.cells[row][col] == 1):
                        botch.add(4, pyglet.gl.GL_QUADS, None, ('v2i', cell_coords), ('c4B', un))
                    elif (self.cells[row][col] == 2):
                        botch.add(4, pyglet.gl.GL_QUADS, None, ('v2i', cell_coords), ('c4B', red))
                    elif (self.cells[row][col] == 3):
                        botch.add(4, pyglet.gl.GL_QUADS, None, ('v2i', cell_coords), ('c4B', trois))
                    elif (self.cells[row][col] > 3):
                        botch.add(4, pyglet.gl.GL_QUADS, None, ('v2i', cell_coords), ('c4B', deux))
                    
                    
        botch.draw()