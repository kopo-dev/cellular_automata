import random
from Grid import Grid

class ForestFire():
    def __init__(self, width, height, cell_size):
        self.grid = Grid(width, height, cell_size)
        self.generate_cells()
        self.grid.draw()

    def generate_cells(self):
        for row in range(0, self.grid.grid_height):
            self.grid.cells.append([])
            for col in range(0, self.grid.grid_width):
                if random.random() < 0.1:
                    self.grid.cells[row].append(1)
                else:
                    self.grid.cells[row].append(0)

    def run_rules(self):
        new_grid = []
        for row in range(0, self.grid.grid_height):
            new_grid.append([])
            for col in range(0, self.grid.grid_width):
                new_grid[row].append(0)
                neighbours = self.grid.getNeightbours(col, row)
                burning = neighbours.count(2)
                lighting = random.random()
                new_tree = random.random()
                if (self.grid.cells[row][col] == 0):
                    if (new_tree > 0.99):
                        new_grid[row][col] = 1
                elif(self.grid.cells[row][col] == 1):
                    if (burning > 0):
                        new_grid[row][col] = 2
                    elif(lighting > 0.999):
                        new_grid[row][col] = 2
                    else:
                        new_grid[row][col] = 1
                else:
                    new_grid[row][col] = 0
        self.grid.cells = new_grid
        
